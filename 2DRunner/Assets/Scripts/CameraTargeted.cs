﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTargeted : MonoBehaviour {

	[SerializeField]
	private Transform TargetObject;
	[SerializeField]
	private Vector3 Offset;

	void Start()
	{
		transform.position = TargetObject.position + Offset;
	}

	// Update is called once per frame
	void Update ()
	{
		transform.position = new Vector3(TargetObject.position.x + Offset.x, transform.position.y, transform.position.z);
	}
}
