﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
	protected Renderer m_bodyRenderer;
	public int ScorePoints = 1;
	//for external affects
	private float m_scrollSpeed = 0f;

	protected virtual void Start()
	{
		m_scrollSpeed = GlobalVariables.ScrollSpeed;
		m_bodyRenderer = GetComponentInChildren<Renderer>();
		EventManager.StartListening("Pause", ()=> m_scrollSpeed = 0f);
		EventManager.StartListening("Resume", () => m_scrollSpeed = GlobalVariables.ScrollSpeed);
	}

	private void OnDestroy()
	{
		EventManager.StopListening("Pause", () => m_scrollSpeed = 0f);
		EventManager.StopListening("Resume", () => m_scrollSpeed = GlobalVariables.ScrollSpeed);
	}

	void Update()
	{
		transform.Translate(-transform.right * m_scrollSpeed * Time.deltaTime);
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag.Contains("Player"))
		{
			EventManager.TriggerEvent("Restart");
		}
		else if (collision.tag.Contains("ObstacleCounter"))
		{
			GameDirector.AddScore(ScorePoints);
		}
		else if (collision.tag.Contains("ObstacleHider"))
		{
			Hide();
		}
	}

	protected void Hide()
	{
		gameObject.SetActive(false);
	}

	public void Reset()
	{
		transform.localScale = Vector3.one;
		transform.localPosition = new Vector3();
		transform.localRotation = Quaternion.identity;
	}
}
