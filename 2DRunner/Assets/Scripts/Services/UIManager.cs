﻿using UnityEngine.UI;
using UnityEngine;

public struct UILeaderboardEntry
{
	public GameObject Container;
	public Text Number;
	public Text Name;
	public Text Score;

	public UILeaderboardEntry(GameObject other)
	{
		Container = other;
		// its better to crash if can't find any of this elements
		Number = other.transform.Find("Number").GetComponent<Text>();
		Name = other.transform.Find("Name").GetComponent<Text>();
		Score = other.transform.Find("Score").GetComponent<Text>();
	}

	public void SetRecord(Record record, int position)
	{
		Number.text = position.ToString();
		Name.text = record.Name;
		Score.text = record.Score.ToString();
	}
}

public class UIManager : Singleton<UIManager>
{
	[SerializeField]
	private Text m_textCurrentScore;
	[SerializeField]
	private Button m_menu;
	[SerializeField]
	private Button m_newGame;
	[SerializeField]
	private Button m_continue;
	[SerializeField]
	private Button m_leaderboard;
	[SerializeField]
	private GameObject m_leaderboardEntry;
	private GameObject m_leaderboardTable;

	// needs to refresh table
	private UILeaderboardEntry m_sampleEntry;

	private void Awake()
	//void Start()
	{
		if (m_leaderboardEntry && m_leaderboardEntry.transform.parent)
		{
			m_leaderboardTable = m_leaderboardEntry.transform.parent.gameObject;
			m_sampleEntry = new UILeaderboardEntry(m_leaderboardEntry);
			m_sampleEntry.Container.transform.SetParent(null);
		}

		m_newGame.onClick.AddListener(OnNewGame);
		m_continue.onClick.AddListener(OnContinue);
		m_menu.onClick.AddListener(OnMenu);
		m_leaderboard.onClick.AddListener(OnLeaderboard);

		EventManager.StartListening("Pause", OnPause);
		EventManager.StartListening("Resume", OnResume);
		EventManager.StartListening("Leaderboard", ShowLeaderboard);
	}

	private void OnDestroy()
	{
		EventManager.StopListening("Pause", OnPause);
		EventManager.StopListening("Resume", OnResume);
		EventManager.StartListening("Leaderboard", ShowLeaderboard);
	}

	void ShowLeaderboard()
	{
		RefreshLeaderboard();

		m_textCurrentScore.gameObject.SetActive(false);
		m_menu.gameObject.SetActive(true);

		m_newGame.gameObject.SetActive(false);
		m_continue.gameObject.SetActive(false);
		m_leaderboard.gameObject.SetActive(false);

		m_leaderboardTable.gameObject.SetActive(true);
	}

	void RefreshLeaderboard()
	{
		var childrenCount = m_leaderboardTable.transform.childCount;
		foreach (Transform child in m_leaderboardTable.transform)
			Destroy(child.gameObject);

		var rt = m_sampleEntry.Container.GetComponent<RectTransform>();
		rt.Translate(0f, rt.rect.height * childrenCount, 0f);
		int index = 1;
		foreach ( var rec in Leaderboard.Instance.leaderboards )
		{
			rt.Translate(0f, -rt.rect.height, 0f);

			var newEntry = new UILeaderboardEntry(Instantiate(m_sampleEntry.Container));
			newEntry.SetRecord(rec, index);
			newEntry.Container.transform.SetParent(m_leaderboardTable.transform);
			index++;
		}
	}

	void OnPause()
	{
		m_textCurrentScore.gameObject.SetActive(false);
		m_menu.gameObject.SetActive(false);

		m_newGame.gameObject.SetActive(true);
		m_continue.gameObject.SetActive(true);
		m_leaderboard.gameObject.SetActive(true);

		m_leaderboardTable.gameObject.SetActive(false);
	}

	void OnResume()
	{
		m_textCurrentScore.gameObject.SetActive(true);
		m_menu.gameObject.SetActive(true);

		m_newGame.gameObject.SetActive(false);
		m_continue.gameObject.SetActive(false);
		m_leaderboard.gameObject.SetActive(false);

		m_leaderboardTable.gameObject.SetActive(false);
	}

	void Update()
	{
		

		if (Input.GetKeyDown(KeyCode.Escape))
			Application.Quit();
	}

	public static void UpdateCurrentScore(int currentScore)
	{
		Instance.m_textCurrentScore.text = string.Format("SCORE: {0}", currentScore);
	}

	private void OnNewGame()
	{
		EventManager.TriggerEvent("Restart");
		EventManager.TriggerEvent("Resume");
	}

	private void OnContinue()
	{
		EventManager.TriggerEvent("Resume");
	}

	private void OnMenu()
	{
		EventManager.TriggerEvent("Pause");
	}

	private void OnLeaderboard()
	{
		EventManager.TriggerEvent("Leaderboard");
	}
}
