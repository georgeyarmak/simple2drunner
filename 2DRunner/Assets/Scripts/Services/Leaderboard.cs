﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class Record
{
	public string Name = "";
	public int Score = 0;

	public Record(string name, int score)
	{
		Name = name;
		Score = score;
	}
}

public class Leaderboard : Singleton<Leaderboard>
{
	public List<Record> leaderboards = new List<Record>();

	const string k_lbPostURL = "http://www.site.com/lb/post";
	const string k_lbGetURL = "http://www.site.com/lb/get";

	private void Awake()
	{
		//EventManager.StartListening("Leaderboard", ()=> GetTable());
		//EventManager.StartListening("Restart", () => PostScore());
	}

	private void OnDestroy()
	{
		//EventManager.StopListening("Leaderboard", () => GetTable());
		//EventManager.StopListening("Restart", () => PostScore());
	}

	private void Start()
	{
		leaderboards.Clear();
		leaderboards.Add(new Record("Oleg", 23));
		leaderboards.Add(new Record("Alex", 15));
		leaderboards.Add(new Record("Roman", 10));
	}

	// Use this for initialization
	IEnumerator PostScore()
	{
		var playerEntry = leaderboards.Find(rec => rec.Name == GameDirector.Instance.PlayerName);
		if(playerEntry == null)
		{
			playerEntry = new Record(GameDirector.Instance.PlayerName, GameDirector.Instance.CurrentScore);
			leaderboards.Add(playerEntry);
		}
		else
		{
			playerEntry.Score = GameDirector.Instance.CurrentScore;
		}

		var json = JsonUtility.ToJson(playerEntry);
		var download = UnityWebRequest.Post(k_lbPostURL, json);

		// Wait until the download is done
		yield return download.SendWebRequest();

		if (download.isNetworkError || download.isHttpError)
		{
			print("Error downloading: " + download.error);
		}
	}

	IEnumerator GetTable()
	{
		var download = UnityWebRequest.Get(k_lbGetURL);
		yield return download.SendWebRequest();

		if (download.isNetworkError || download.isHttpError)
		{
			print("Error downloading: " + download.error);
		}
		else
		{
			JsonUtility.FromJsonOverwrite(download.downloadHandler.data.ToString(), leaderboards);
			leaderboards.Sort((rec1, rec2) => rec1.Score.CompareTo(rec2.Score));
		}
	}
}