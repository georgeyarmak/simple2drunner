﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalVariables : ScriptableObject
{
	public static float ScrollSpeed = 5f;
	public static float JumpForce = 800f;

	private static Camera m_cameraMain;
	public static Camera CameraMain
	{
		get
		{
			if (m_cameraMain == null)
				m_cameraMain = Camera.main;

			return m_cameraMain;
		}
	}

	private static GameObject m_player;
	public static GameObject Player
	{
		get
		{
			if (m_player == null)
				m_player = GameObject.FindGameObjectsWithTag("Player")[0];

			return m_player;
		}
	}
}