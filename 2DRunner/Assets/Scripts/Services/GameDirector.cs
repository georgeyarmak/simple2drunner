﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
	k_none = -1,
	k_menu,
	k_action,
	k_leaderboard
}


public class GameDirector : Singleton<GameDirector>
{
	private ObstacleSpawner[] m_genericSpawner;
	private int m_currentScore = 0;
	public int CurrentScore { get { return m_currentScore; } set { m_currentScore = value; } }

	private float m_originalScrollSpeed = 0f;
	private GameState m_gameState = GameState.k_none;

	public bool IsInGame()
	{
		return m_gameState == GameState.k_action;
	}

	public bool IsOnLeaderboard()
	{
		return m_gameState == GameState.k_leaderboard;
	}

	public string PlayerName = "NewPlayer";

	void Start()
	{
		m_genericSpawner = FindObjectsOfType<ObstacleSpawner>();
		m_originalScrollSpeed = GlobalVariables.ScrollSpeed;

		EventManager.StartListening("Restart", ResetGame);
		EventManager.StartListening("Pause", () => m_gameState = GameState.k_menu);
		EventManager.StartListening("Resume", () => m_gameState = GameState.k_action);
		EventManager.StartListening("Leaderboard", () => m_gameState = GameState.k_leaderboard);

		ResetGame();
	}

	private void OnDestroy()
	{
		EventManager.StopListening("Restart", ResetGame);
		EventManager.StopListening("Pause", () => m_gameState = GameState.k_menu);
		EventManager.StopListening("Resume", () => m_gameState = GameState.k_action);
		EventManager.StopListening("Leaderboard", () => m_gameState = GameState.k_leaderboard);
	}

	void Update()
	{
		if (IsInGame())
		{
			GlobalVariables.ScrollSpeed += Time.deltaTime / 5f;
		}
	}

	public static void AddScore(int sum)
	{
		Instance.m_currentScore += sum;

		UIManager.UpdateCurrentScore(Instance.m_currentScore);
	}

	public static void ShowScoreResults()
	{
		for (int i = 0; i < Instance.m_genericSpawner.Length; i++)
		{
			Instance.m_genericSpawner[i].Stop();
		}
	}

	public static void ResetGame()
	{
		for (int i = 0; i < Instance.m_genericSpawner.Length; i++)
		{
			Instance.m_genericSpawner[i].Reload();
		}

		GlobalVariables.ScrollSpeed = Instance.m_originalScrollSpeed;

		Instance.m_currentScore = 0;
		UIManager.UpdateCurrentScore(Instance.m_currentScore);
		EventManager.TriggerEvent("Pause");
	}
}