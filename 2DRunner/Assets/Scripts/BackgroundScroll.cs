﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroll : MonoBehaviour {

	void Update ()
	{
		if (!GameDirector.Instance.IsInGame())
			return;

		Vector2 offset = new Vector2(Time.time * GlobalVariables.ScrollSpeed/10, 0);
		GetComponent<Renderer>().material.mainTextureOffset = offset;
	}
}
