﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : GenericPooling
{
	[SerializeField]
	private float m_minSpawnTime = 1, m_maxSpawnTime = 1;
	private float m_originalMinSpawnTime, m_originalMaxSpawnTime, m_limitMinSpawnTime, m_limitMaxSpawnTime;

	void Start()
	{
		m_originalMinSpawnTime = m_minSpawnTime;
		m_originalMaxSpawnTime = m_maxSpawnTime;

		m_limitMinSpawnTime = m_originalMinSpawnTime / 2f;
		m_limitMaxSpawnTime = m_originalMaxSpawnTime / 2f;

		InvokeSpawn();
	}


	public void Reload()
	{
		DeactivateAll();

		m_minSpawnTime = m_originalMinSpawnTime;
		m_maxSpawnTime = m_originalMaxSpawnTime;

		InvokeSpawn();
	}

	public void Stop()
	{
		CancelInvoke("Spawn");
	}

	private void Spawn()
	{
		if (GameDirector.Instance.IsInGame())
		{
			var obj = GetObjectFromPool();
			if (obj)
			{
				var newObs = obj.GetComponent<Obstacle>();
				if (newObs != null)
					newObs.Reset();
			}
		}

		InvokeSpawn();
	}

	private void InvokeSpawn()
	{
		var time = Random.Range(m_minSpawnTime, m_maxSpawnTime);

		var minus = Time.deltaTime / 5f;
		if (m_minSpawnTime > m_limitMinSpawnTime) m_minSpawnTime -= minus;
		if (m_maxSpawnTime > m_limitMaxSpawnTime) m_maxSpawnTime -= minus;

		Invoke("Spawn", time);
	}
}