using System;
using UnityEngine;

public class RunnerCharacter2D : MonoBehaviour
{
	const float k_GroundedRadius = .2f;

	private Transform m_GroundCheck;
	private bool m_Grounded = false;
	private Animator m_Anim;
	private Rigidbody2D m_Rigidbody2D;
	private bool m_doubleJump = false;

	private void Awake()
	{
		// Setting up references.
		m_GroundCheck = transform.Find("GroundCheck");
		m_Anim = GetComponent<Animator>();
		m_Rigidbody2D = GetComponent<Rigidbody2D>();

		EventManager.StartListening("Pause", OnPause);
		EventManager.StartListening("Resume", OnResume);
	}

	private void OnDestroy()
	{
		EventManager.StopListening("Pause", OnPause);
		EventManager.StopListening("Resume", OnResume);
	}

	private void OnPause()
	{
		m_Anim.enabled = false;
		m_Rigidbody2D.Sleep();
	}

	private void OnResume()
	{
		m_Anim.enabled = true;
		m_Rigidbody2D.WakeUp();
	}

	private void FixedUpdate()
	{
		m_Grounded = false;
		Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius);
		foreach(var collider in colliders)
		{
			if(collider.tag.Contains("Obstacle"))
			{
				EventManager.TriggerEvent("Restart");
				return;
			}

		    if (collider.gameObject != gameObject)
		        m_Grounded = true;
		}

		if (m_Grounded)
			m_doubleJump = false;

		m_Anim.SetBool("Ground", m_Grounded);
		m_Anim.SetFloat("vSpeed", m_Rigidbody2D.velocity.y);
	}
	
	
	public void Move(bool jump)
	{
		if (m_Grounded)
		{
			m_Anim.SetFloat("Speed", Mathf.Abs(GlobalVariables.ScrollSpeed));
		}
		// If the player should jump...
		if ((!m_doubleJump || m_Grounded) && jump)
		{
			m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, 0f);
			m_Rigidbody2D.AddForce(new Vector2(0f, GlobalVariables.JumpForce));

			if (!m_Grounded)
				m_doubleJump = true;
		}
	}
}
