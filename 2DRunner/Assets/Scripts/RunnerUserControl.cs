using System;
using UnityEngine;

[RequireComponent(typeof (RunnerCharacter2D))]
public class RunnerUserControl : MonoBehaviour
{
    private RunnerCharacter2D m_Character;
    private bool m_Jump;


    private void Awake()
    {
        m_Character = GetComponent<RunnerCharacter2D>();
    }


    private void Update()
    {
		if (!GameDirector.Instance.IsInGame())
			return;

        if (!m_Jump)
        {
            // Read the jump input in Update so button presses aren't missed.
            m_Jump = Input.GetButtonUp("Jump");
        }
	}


    private void FixedUpdate()
    {
		m_Character.Move(m_Jump);
        m_Jump = false;
    }
}
